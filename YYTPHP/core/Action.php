<?php
/**
 *------------------------------------------------
 * Author: YYT[QQ:375776626]
 *------------------------------------------------
 */

abstract class Action extends Template
{
    protected $var = array(); //提供一个默认的模板变量

    public function __construct()
    {
        $this->var['template_url'] = Web::getTemplateUrl();
    }

    public function display($template = '')
    {
        if (!$template) {
            $template = strtolower(substr(get_class($this), 0, -6));
            $action = get_called_method();
            if ($action != 'index' && $template != '_empty') $template .= '/'.$action;
            if ($template == '_empty') $template = $action;
        }
        $this->assign('var', $this->var);
        parent::display($template);
    }

    private function _info($type, $content, $backUrl = '', $template = '')
    {
        $title = $type == 'success' ? '操作成功' : '操作失败';
        if (!$backUrl) {
            $backUrl = $type == 'success' ? $_SERVER['HTTP_REFERER'] : 'javascript:history.back(-1);';
            $backUrl = array('返回上页' => $backUrl);
        }
        if (!$template) $template = $type;
        $this->var['page_title'] = $title;
        $this->var['content'] = $content;
        $this->var['back_urls'] = $backUrl;
        $this->assign('var', $this->var);
        exit($this->fetch($template));
    }

    protected function success($content, $backUrl = '', $template = '')
    {
        $this->_info(__FUNCTION__, $content, $backUrl, $template);
    }

    protected function error($content, $backUrl = '', $template = '')
    {
        $this->_info(__FUNCTION__, $content, $backUrl, $template);
    }

    protected function tips($type, $tips, $template = '')
    {
        $this->var[$type] = $tips;
        $this->assign('var', $this->var);
        if ($template) echo $this->fetch($template);
    }
}

if (!function_exists('get_called_method')) {
    function get_called_method()
    {
        $backtrace = debug_backtrace();
        array_shift($backtrace);
        return $backtrace[1]['function'];
    }
}