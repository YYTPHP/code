<?php
/**
 *------------------------------------------------
 * Author: YYT[QQ:375776626]
 *------------------------------------------------
 */

return array(
    'debug'                     => false,

    'db_driver'                 => 'PDO',
    'db_type'                   => '',
    'db_dsn'                    => '',
    'db_host'                   => 'localhost',
    'db_port'                   => 3306,
    'db_name'                   => '',
    'db_user'                   => 'root',
    'db_password'               => '',
    'db_charset'                => 'UTF8',
    'db_long_connect'           => false,
    'db_prefix'                 => '',
    'db_prefix_var'             => '@_',
    'db_memcache'               => array(
        //array('localhost', '11211'),
        //array('localhost', '11211'),
    ),

    'template_path'                 => ROOT_PATH.'/template',
    'template_compile_path'         => ROOT_PATH.'/data/temp',
    'template_suffix'               => '.html',
    'template_cache_path'           => '',
    'template_cache_name'           => '',
    'template_caching'              => false,
    'template_left_delimiter'       => '{',
    'template_right_delimiter'      => '}',

    'cache_path'                => ROOT_PATH.'/data/cache',
    'model_path'                => ROOT_PATH.'/model',
    'log_path'                  => ROOT_PATH.'/data/log',

    'url_space'                 => '/',
    'url_suffix'                => '.html',
    'url_rewrite'               => false,
    'url_base_path'             => '',

    'session_prefix'            => 'YYTPHP_',

    'cookie_expire'             => 12, //过期时效(小时)
    'cookie_path'               => '/',
    'cookie_domain'             => '',
    'cookie_secure'             => false, //是否通过安全的 HTTPS 连接来传输 cookie


    'timezone'                  => 8, //时差
    'gzip'                      => true, //页面压缩(如页面不显示请关闭)
);