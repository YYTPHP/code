<?php
if (PHP_VERSION < '5.3') exit('PHP_VERSION >= 5.3');
/**
 * 配置文件位置及名称: config/db/dbname.php
 * 配置如下:
 * return array(
 *     'db_driver'         => 'mysqli',
 *     'db_type'           => 'mysql',
 *     'db_long_connect'   => false,
 *     'db_name'           => basename(__file__, '.php'), //dbname
 *     'db_host'           => 'localhost',
 *     'db_user'           => 'root',
 *     'db_password'       => 'root',
 * );
 * 用法:
 * Data::init(); //初始化配置
 * Data::dbname('tablename')->fetch();
 */
class Data
{
    public static function __callStatic($configFileName, $table)
    {
        $table = reset($table);
        $dbs = Web::config('dbs');
        Web::config($dbs[$configFileName]);
        return Web::db($table);
    }

    public static function init()
    {
        $configFiles = glob(ROOT_PATH.'/config/db/*.php');
        if ($configFiles) {
            $result = array();
            foreach ($configFiles as $file) {
                $result['dbs'][basename($file, '.php')] = require $file;
            }
            Web::config($result);
        }
    }
}