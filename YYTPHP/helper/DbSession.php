<?php
/**
 * 用数据库保存SESSION(基于YYTPHP)
 * 需要的字段 session_id[pk varchar(255)] session_expires[int(10)] session_data[text]
 * @param 数据库对象
 * eg:
    ini_set('session.save_handler', 'user');
    $DbSession = new DbSession(Web::db('session'));
    session_set_save_handler(
    array($DbSession, 'open'),
    array($DbSession, 'close'),
    array($DbSession, 'read'),
    array($DbSession, 'write'),
    array($DbSession, 'destroy'),
    array($DbSession, 'gc')
    );
 */
class DbSession
{
    private $_db;

    private $_lifeTime;

    public function __construct($db)
    {
        $this->_db = $db;
        $this->_lifeTime = get_cfg_var('session.gc_maxlifetime');
    }

    public function open()
    {
        return true;
    }

    public function read($sessID)
    {
        $where['session_id'] = $sessID;
        $where['session_expires >'] = time();
        $session = $this->_db->where($where)->field('session_data')->fetch();
        if ($session) return $session['session_data'];
        return '';
    }

    public function write($sessID, $sessData)
    {
        $newExp = time() + $this->_lifeTime;

        $session = $this->_db->where(array('session_id' => $sessID))->fetch();
        $data['session_expires'] = $newExp;
        $data['session_data'] = $sessData;
        if ($session) {
            return $this->_db->where(array('session_id' => $sessID))->update($data);
        } else {
            $data['session_id'] = $sessID;
            return $this->_db->insert($data);
        }
        return false;
    }

    public function destroy($sessID)
    {
        return $this->_db->where(array('session_id' => $sessID))->delete();
    }

    public function gc($sessMaxLifeTime)
    {
        $where['session_expires <'] = time();
        return $this->_db->where($where)->delete();
    }

    public function close()
    {
        return $this->gc(ini_get('session.gc_maxlifetime'));
    }
}