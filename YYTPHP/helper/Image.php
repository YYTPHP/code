<?php

/**
 *-------------------------------------------------------------------------------
 * YuPHP 1.0 图片处理类文件
 *-------------------------------------------------------------------------------
 * 版权所有: CopyRight By Mister Yu
 * 您可以自由使用该源码，但是在使用过程中，请保留作者信息。尊重他人劳动成果就是尊重自己
 *-------------------------------------------------------------------------------
 * Url: http://www.yuphp.org
 * Author: Mister Yu
 * Time: 2011-12-31
 *-------------------------------------------------------------------------------
 */

class Image {

	/**
	 * 生成验证码图片
	 *
	 * @access public
	 * @param int $length	验证码中字符的个数
	 * @param int $type		字符的选择库（数字、小写字母、大写字母、前三种混合）
	 * @param string $name	验证码字符串在 Session 中保存的键名
	 */
	public static function verify($length = 4, $type = 4, $name = 'verify') {
		header('Content-Type:image/jpeg');
		if ($length < 3 || $length > 8) $length = 4;

		switch ($type) {
			case 1 :
				$vfyStr = "23456789";
				break;
			case 2 :
				$vfyStr = "abcdefghjkmnpqrstwxyz";
				break;
			case 3 :
				$vfyStr = "ABCDEFGHJKMNPQRSTWXYZ";
				break;
			default :
				$vfyStr = "23456789abcdefghjkmnpqrstwxyzABCDEFGHJKMNPQRSTWXYZ";
				break;
		}

		$vfyChar = substr(str_shuffle($vfyStr), 0, $length);

        Web::session($name, strtolower($vfyChar));

		$vfyWidth = $length * 20;
		$vfyHeight = 16;
		$vfyImage = imagecreate($vfyWidth, $vfyHeight);
		imagecolorallocate($vfyImage, 255, 255, 255);

		for ($i = 0; $i < $length; $i++) {
			$vfyColor = imagecolorallocate($vfyImage, rand(0, 100), rand(0, 150), rand(0, 200));
			imagestring($vfyImage, 5, $vfyWidth * $i / $length + rand(0, 10), rand(0, $vfyHeight - 15), $vfyChar[$i], $vfyColor);
		}

		$dotNumber = $length * 20;
		for ($i = 0; $i < $dotNumber; $i++) {
			$dotColor = imagecolorallocate($vfyImage, rand(0, 255), rand(0, 255), rand(0, 255));
			imagesetpixel($vfyImage, rand(0, $vfyWidth), rand(0, $vfyHeight), $dotColor);
		}

		$lineNumber = $length / 2;
		for ($i = 0; $i < $lineNumber; $i++) {
			$lineColor = imagecolorallocate($vfyImage, rand(0, 255), rand(0, 255), rand(0, 255));
			imageline($vfyImage, rand(0, $vfyWidth), rand(0, $vfyHeight), rand(0, $vfyWidth), rand(0, $vfyHeight), $lineColor);
		}

		imagejpeg($vfyImage);
		imagedestroy($vfyImage);
	}

	/**
	 * 给图片加上文字
	 *
	 * @access public
	 * @param string $dstUrl	已有的图片地址
	 * @param string $str		要写在图片上的文字
	 * @param int $place		文字写在图片上的位置（1为左上角、2为右上角、3为左下角、4为右下角）
	 * @param int $cover		是否删除原来的图片（0为不删除、1为删除）
	 * @return string
	 */
	public static function strMark($dstUrl, $str, $place = 4, $cover = 1) {
		$dstImage = self::create($dstUrl);

		$strlen = strlen($str);
		$length = 0;
		for ($i = 0; $i < $strlen; $i++) {
			if (ord($str[$i] < 0x81)) {
				$length++;
			} else {
				$length += 2;//中文字符
			}
		}
		$dstSize = getimagesize($dstUrl);

		switch ($place) {
			case 1 :
				$placeWidth = 25;
				$placeHeight = 15;
				break;
			case 2 :
				$placeWidth = $dstSize[0] - $length * 10 - 25;
				$placeHeight = 15;
				break;
			case 3 :
				$placeWidth = 25;
				$placeHeight = $dstSize[1] - 35;
				break;
			default :
				$placeWidth = $dstSize[0] - $length * 10 - 25;
				$placeHeight = $dstSize[1] - 35;
				break;
		}

		$str = iconv('GBK', 'UTF-8', $str);
		$color = imagecolorallocate($dstImage, rand(0, 255), rand(0, 255), rand(0, 255));
		imagestring($dstImage, 10, $placeWidth, $placeHeight, $str, $color);
		//imagettftext($dstImage, 10, 0, $placeWidth, $placeHeight, $color, dirname(__FILE__).'/simhei.ttf', $str);

		if ($cover == 1) unlink($dstUrl);

		$newUrl = substr($dstUrl, 0, -strlen(end(explode('.', $dstUrl)))).'jpg';
		imagejpeg($dstImage, $newUrl);
		imagedestroy($dstImage);

		return $newUrl;
	}

	/**
	 * 给图片加水印
	 *
	 * @access public
	 * @param string $dstUrl	被加水印的图片地址
	 * @param string $srcUrl	水印图片地址
	 * @param int $space		水印位置
	 * @param int $cover		是否删除原来的 dstUrl 图片
	 * @return string
	 */
	public static function waterMark($dstUrl, $srcUrl, $place = 4, $cover = 1) {
		$dstImage = self::create($dstUrl);
		$srcImage = self::create($srcUrl);
		$dstSize = getimagesize($dstUrl);
		$srcSize = getimagesize($srcUrl);

		switch ($place) {
			case 1 :
				$placeWidth = 25;
				$placeHeight = 15;
				break;
			case 2 :
				$placeWidth = $dstSize[0] - $srcSize[0] - 25;
				$placeHeight = 15;
				break;
			case 3 :
				$placeWidth = 25;
				$placeHeight = $dstSize[1] - $srcSize[1] - 35;
				break;
			default :
				$placeWidth = $dstSize[0] - $srcSize[0] - 25;
				$placeHeight = $dstSize[1] - $srcSize[1] - 35;
				break;
		}

		imagecopy($dstImage, $srcImage, $placeWidth, $placeHeight, 0, 0, $srcSize[0], $srcSize[1]);

		if ($cover == 1) unlink($dstUrl);

		$newUrl = substr($dstUrl, 0, -strlen(end(explode('.', $dstUrl)))).'jpg';
		imagejpeg($dstImage, $newUrl);

		imagedestroy($dstImage);
		imagedestroy($srcImage);

		return $newUrl;
	}

	/**
	 * 裁剪图片大小
	 *
	 * @access public
	 * @param string $dstUrl	图片的地址
	 * @param int $newWidth		宽度
	 * @param int $newHeight	高度
	 * @return string
	 */
	public static function cut($dstUrl, $width = 320, $height = 240)
	{
		if (!file_exists($dstUrl)) {
			return false;
		}
		$dstSize = getimagesize($dstUrl);
		$dstImage = self::create($dstUrl);
		$srcw = imagesx($dstImage);
		$srch = imagesy($dstImage);
		$towh = $width / $height;
		$srcwh = $srcw / $srch;
		if ($towh <= $srcwh) {
			$ftow = $width;
			@$ftoh = $ftow * ($srch / $srcw);
		} else {
			$ftoh = $height;
			@$ftow = $ftoh * ($srcw / $srch);
		}
		if ($srcw > $width || $srch > $height) {
			if (function_exists('imagecreatetruecolor')) {
				if (@$ni = imagecreatetruecolor($ftow, $ftoh)) {
					imagecopyresampled($ni, $dstImage, 0, 0, 0, 0, $ftow, $ftoh, $srcw, $srch);
				} else {
					$ni = imagecreate($ftow, $ftoh);
					imagecopyresized($ni, $dstImage, 0, 0, 0, 0, $ftow, $ftoh, $srcw, $srch);
				}
			} else {
				$ni = imagecreate($ftow, $ftoh);
				imagecopyresized($ni, $dstImage, 0, 0, 0, 0, $ftow, $ftoh, $srcw, $srch);
			}
			if (function_exists('imagejpeg')) {
				ImageJpeg($ni, $dstUrl);
			} else {
				imagepng($ni, $dstUrl);
				imagedestroy($ni);
			}
			imagedestroy($dstImage);
		}
		return $dstUrl;
	}

	/**
	 * 根据图片类型选择函数打开图片
	 *
	 * @access public
	 * @param string $imageUrl	图片的地址
	 * @param resource
	 */
	public static function create($imageUrl) {
		$imageSize = getimagesize($imageUrl);
		switch ($imageSize[2]) {
			case 1 :
				$image = imagecreatefromgif($imageUrl);
				break;
			case 2 :
				$image = imagecreatefromjpeg($imageUrl);
				break;
			case 3 :
				$image = imagecreatefrompng($imageUrl);
				break;
			default :
				exit('不支持编辑此类图片文件！');
				break;
		}

		return $image;
	}

}