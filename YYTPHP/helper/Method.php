<?php
/**
 *------------------------------------------------
 * Author: YYT[QQ:375776626]
 *------------------------------------------------
 */

abstract class Method
{
    /**
     * 字符串高亮
     * @param string
     * @param string
     * @param string
     * @param boolean true为UBB模式
     * @return string
     */
    public static function highlight($content, $key, $color = 'red', $isUbb = '')
    {
        if (empty($key)) return $content;
        $kFi = substr($key, 0, 1);
        $kLen = strlen($key);
        $lLen = strlen($content);
        $con = '';
        for ($ln = 0; $ln < $lLen; $ln++) {
            $ls = substr($content, $ln, 1);
            if ($ls == '<') {
                while ($ls != '>') {
                    $con .= $ls;
                    $ln++;
                    $ls = substr($content, $ln, 1);
                }
                $con .= $ls;
            } else if (strtolower($ls) == strtolower($kFi)) {
                $lKey = substr($content, $ln, $kLen);
                if (strtolower($lKey) != strtolower($key)) {
                    $con .= $ls;
                } else {
                    $ln += $kLen -1;
                    $con .= !empty($isUbb) ? '[color='.$color.'][b]' : '<strong><font color="'.$color.'">';
                    $con .= $lKey;
                    $con .= !empty($isUbb) ? '[/b][/color]' : '</font></strong>';
                }
            } else {
                $con .= $ls;
            }
        }
        return $con;
    }

    /**
     * 字符串转数组
     * @param string
     * @param string 分隔符
     * @return array
     */
    public static function str2array($str, $depr = ',')
    {
        return explode($depr, $str);
    }

    /**
     * 数组转字符串
     * @param array
     * @param string 分隔符
     * @return string
     */
    public static function array2str($array, $depr = ',')
    {
        if (is_array($array)) {
            $array = array_unique($array);
            return join($depr, $array);
        }
        return $array;
    }

    /**
     * 过滤HTML代码
     * @param string
     * @param string 保留的标签
     * @return string
     */
    public static function clearHtml($html, $allow = '')
    {
        $html = html_entity_decode($html);
        return strip_tags($html, $allow);
    }

    /**
     * 还原HTML代码
     * @param string
     * @return string
     */
    public static function backHtml($html)
    {
        return html_entity_decode($html);
    }

    /**
     * 二维数组排序
     * @param array
     * @param string 需要排序的键值
     * @param string ASC 正序 DESC 倒序
     * @return array
     */
    public static function multiArraySort($multiArray, $sortKey, $sort = 'DESC')
    {
        if (is_array($multiArray)) {
            foreach ($multiArray as $row) {
                if (is_array($row)) {
                    $keyArray[] = $row[$sortKey];
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
        $sort = strtolower($sort) == 'desc' ? 3 : 4;
        array_multisort($keyArray, $sort, $multiArray);
        return $multiArray;
    }

    /**
     * 格式化金额
     * @param float 金额
     * @param int 尾数
     * @param bool 是否四舍五入
     * @return float
     */
    public static function formatMoney($money, $mantissa = 2, $isRound = false)
    {
        if (!$isRound) {
            $s = '-'.$mantissa;
            $n = $mantissa + $mantissa;
            return sprintf("%.{$mantissa}f", substr(sprintf("%.{$n}f", $money), 0, (int)$s));
        }
        return sprintf("%.{$mantissa}f", $money);
    }

    public static function formatTime($time)
    {
        $time -= 1;
        if ($time > time()) {
            $t = $time - time();
            $suffix = '后';
        } else {
            $t = time() - $time;
            $suffix = '前';
        }
        $f = array(
            '31536000' => '年',
            '2592000' => '个月',
            '604800' => '星期',
            '86400' => '天',
            '3600' => '小时',
            '60' => '分钟',
            '1' => '秒'
        );
        foreach ($f as $k => $v) {
            if (0 != $c = floor($t / (int)$k)) {
                $m = floor($t % $k);
                foreach ($f as $x => $y) {
                    if (0 != $r = floor($m / (int)$x)) return $c.$v.$r.$y.$suffix;
                }
                return $c.$v.$suffix;
            }
        }
    }

    public static function formatByte($size, $dec = 2)
    {
        $a = array('B', 'KB', 'MB', 'GB', 'TB', 'PB');
        $pos = 0;
        while ($size >= 1024) {
            $size /= 1024;
            $pos++;
        }
        return round($size, $dec).' '.$a[$pos];
    }

    public static function substr($str, $start = 0, $length, $charset = 'utf-8', $suffix = '...')
    {
        if (self::strlen($str) <= $length) return $str;
        if (function_exists('mb_substr')) {
            return mb_substr($str, $start, $length, $charset).$suffix;
        } else if (function_exists('iconv_substr')) {
            return iconv_substr($str, $start, $length, $charset).$suffix;
        }
        $re['utf-8'] = "/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|[\xe0-\xef][\x80-\xbf]{2}|[\xf0-\xff][\x80-\xbf]{3}/";
        $re['gb2312'] = "/[\x01-\x7f]|[\xb0-\xf7][\xa0-\xfe]/";
        $re['gbk'] = "/[\x01-\x7f]|[\x81-\xfe][\x40-\xfe]/";
        $re['big5'] = "/[\x01-\x7f]|[\x81-\xfe]([\x40-\x7e]|\xa1-\xfe])/";
        preg_match_all($re[$charset], $str, $match);
        $slice = join('', array_slice($match['0'], $start, $length));
        return $slice.$suffix;
    }

    public static function strlen($str)
    {
        preg_match_all('/./us', $str, $match);
        return count($match['0']);
    }

    public static function deleteFile($file)
    {
        if (empty($file)) return false;
        if (@is_file($file)) return @unlink($file);
        $ret = true;
        if ($handle = @opendir($file)) {
            while ($filename = @readdir($handle)) {
                if ($filename == '.' || $filename == '..') continue;
                if (!self::deleteFile($file.'/'.$filename)) $ret = false;
            }
        } else {
            $ret = false;
        }
        @closedir($handle);
        if (file_exists($file) && !rmdir($file)) {
            $ret = false;
        }
        return $ret;
    }

    /**
     * 输出类的方法以及注释(并不会输出逻辑)
     * @param string 类名
     * @return string
     */
    public static function displayClass($className)
    {
        if (is_string($className) && !class_exists($className)) return;

        $view = '<!DOCTYPE html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/><title>'.$className.'</title>';
        $view .= '<style type="text/css">body,h1{ margin:0; padding:0; } body{ background:#80ff80 }</style>';
        $view .= '</head><body>';
        $view .= '<h1 style="background:#9999cc;border-bottom:5px #666699 solid">'.$className.'</h1>';

        $Class = new ReflectionClass($className);

        $methodObjects = $Class->getMethods(ReflectionProperty::IS_PUBLIC);

        array_multisort($methodObjects);
        $view .='<div style="margin:20px;border:1px #666 solid; background:#a6fda6;padding:5px;">';
        foreach ($methodObjects as $methodObject) {
            $view .= '<p><a href="#'.$methodObject->name.'" style="color:blue">'.$methodObject->name.'</a></p>';
        }
        $view .='</div>';

        foreach ($methodObjects as $methodObject) {
            $view .= '<div id="'.$methodObject->name.'" style="margin:20px;border:1px #666 solid; background:#a6fda6">';

            $doc = $methodObject->getDocComment();
            if ($doc) $view .= '<pre style="color:green">&nbsp;&nbsp;&nbsp;&nbsp;'.$methodObject->getDocComment().'</pre>';
            $view .= '<h2>&nbsp;&nbsp;&nbsp;&nbsp;<span style="color:#800000">'.$methodObject->class.'</span>';
            $view .= '::<span style="color:blue">'.$methodObject->name.'</span>(';
            foreach ($methodObject->getParameters() as $parameter) {
                $view .= '<span style="color:blue">$</span><span style="color:#008080">'.$parameter->name.'</span>';
                if ($parameter->isOptional()) {
                    $view .= ' = <span style="color:#ff00ff">\''.$parameter->getDefaultValue().'\'</span>';
                }
                $view .= ', ';
            }
            $view = rtrim($view, ', ');
            $view .= ')</h2>';
            $view .= '</div>';
        }

        $view .= '</body></html>';
        echo $view;
    }

    /**
     * 从一个数组中提取重复的值
     * @param array
     * @return array
     */
    public static function fetchRepeatArray($array)
    {
        //获取去掉重复数据的数组
        $uniqueArr = array_unique($array);
        //获取重复数据的数组
        $repeatArr = array_diff_assoc($array, $uniqueArr);
        return $repeatArr;
    }
}