<?php
/**
 *------------------------------------------------
 * Author: YYT[QQ:375776626]
 *------------------------------------------------
 */

class Page
{
    public $url;

    public $urlType = 'href';

    private $_config = array('count' => '', 'list_num' => '', 'current' => '');

    private $_countPage;

    private $_pageKey = 'p';

    /**
     * 分页构造函数
     * @param int 数据统计
     * @param int 每页显示
     * @param int 当前页标识
     * @param int 当前页
     */
    public function __construct($count, $listNum = 20, $pageKey = 'p', $current = '')
    {
        $this->_pageKey = $pageKey;

        if (!$current) $current = isset($_GET[$this->_pageKey]) && $_GET[$this->_pageKey] >= 1 ? $_GET[$this->_pageKey] : 1;

        $this->_config['count'] = intval($count);
        $this->_config['list_num'] = intval($listNum);
        $this->_config['current'] = intval($current);

        $this->_countPage = ceil($this->_config['count'] / $this->_config['list_num']);
        if ($this->_config['current'] < 1) $this->_config['current'] = 1;
        if ($this->_config['current'] > $this->_countPage) {
            $this->_config['current'] = $this->_countPage;
        }
    }

    public function getCountPage()
    {
        return $this->_countPage;
    }

    public function formatArray($array)
    {
        $start = ($this->_config['current'] - 1) * $this->_config['list_num'];
        if ($start < 0) $start = 0;
        if ($start > $this->_config['count']) {
            $start = $this->_config['count'] - intval(($this->_config['list_num'] / 2) + 1);
        }
        $result = array();
        for ($i = $start; $i< ($start + $this->_config['list_num']); $i++) {
            if (!empty($array[$i])) array_push($result, $array[$i]);
        }
        return $result;
    }

    public function limit()
    {
        $start = ($this->_config['current'] - 1) * $this->_config['list_num'];
        if ($start < 1) $start = 0;
        if ($start > $this->_config['count']) {
            $start = $this->_config['count'] - $this->_config['list_num'];
        }
        return $start.','.$this->_config['list_num'];
    }

    private function _parseUrl($page)
    {
        if (!$this->url) {
            if (substr($_SERVER['QUERY_STRING'], 0, 1) == $this->_pageKey) {
                $space = '?';
            } else {
                $space = empty($_SERVER['QUERY_STRING']) ? '?' : '&amp;';
            }
            $this->url = str_replace($space.$this->_pageKey.'='.$this->_config['current'], '', htmlspecialchars(Web::getDomain().$_SERVER['REQUEST_URI'])).$space.$this->_pageKey.'=';
            $this->url .= '$page';
        }
        return str_replace('$page', intval($page), $this->url);
    }

    public function view()
    {
        $result = '';
        if ($this->_config['count'] > $this->_config['list_num']) {
            for ($i = $this->_config['current'] - 5; $i <= $this->_config['current'] + 5 && $i <= $this->_countPage; $i++) {
                if ($i > 0) {
                    if ($i == $this->_config['current']) {
                        $result .= ' <span class="selected">'.$i.'</span> ';
                    } else {
                        $result .= ' <a '.$this->urlType.'="'.self::_parseUrl($i).'">'.$i.'</a> ';
                    }
                }
            }
            if ($this->_config['current'] > 6) {
                $result = '<a '.$this->urlType.'="'.self::_parseUrl(1).'" title="首页">&laquo;</a>...'.$result;
            }
            if ($this->_config['current'] + 5 < $this->_countPage ) {
                $result .= '...<a '.$this->urlType.'="'.self::_parseUrl($this->_countPage).'" title="尾页">&raquo;</a>';
            }
            $result .= ' <span>'.$this->_config['current'].' / '.$this->_countPage.'</span>';
        }
        return $result;
    }

    public function getAll()
    {
        $result['view'] = $this->view();
        if ($this->_config['count'] > $this->_config['list_num']) {
            $result['prev']['page'] = $this->_config['current'] - 1;
            if ($result['prev']['page'] < 1) $result['prev']['page'] = 1;
            $result['prev']['url'] = self::_parseUrl($result['prev']['page']);
            $result['next']['page'] = $this->_config['current'] + 1;
            if ($result['next']['page'] > $this->_countPage) $result['next']['page'] = $this->_countPage;
            $result['next']['url'] = self::_parseUrl($result['next']['page']);
            $result['first']['page'] = 1;
            $result['first']['url'] = self::_parseUrl($result['first']['page']);
            $result['last']['page'] = $this->_countPage;
            $result['last']['url'] = self::_parseUrl($result['last']['page']);
            $result['count_page'] = $this->_countPage;
            $result['count_data'] = $this->_config['count'];
            $result['current'] = $this->_config['current'];
        }
        return $result;
    }
}