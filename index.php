<?php
/**
 *------------------------------------------------
 * Author: YYT[QQ:375776626]
 *------------------------------------------------
 */

require './YYTPHP/Web.php';

Web::config('debug', true);

//配置网站模板路径
Web::config('template_path', ROOT_PATH.'/demo/template');
//配置模板编译路径
Web::config('template_compile_path', ROOT_PATH.'/demo/temp');

//需开启URL重写模块
Web::config('url_rewrite', false);

//URL参数分隔符
Web::config('url_space', '-');

//运行网站
Web::run(ROOT_PATH.'/demo/controller');